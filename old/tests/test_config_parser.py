import sys
import os
import unittest
import tempfile

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from firewall.dashboard.config_parser import Parser


class ParserTestCase(unittest.TestCase):

    def setUp(self):
        self.tempfile = tempfile.mkstemp()[1]
        self.parser = None

    def tearDown(self):
        os.unlink(self.tempfile)

    def test_config_by_read(self):
        self.parser = Parser(self.tempfile)
        self.assertIsNotNone(self.parser.fd)
        
    def test_config_by_readlines(self):
        self.parser = Parser(self.tempfile)
        self.assertIsNotNone(self.parser.fd)

    def test_config_get_plaintext(self):
        self.parser = Parser(self.tempfile)
        self.parser.fd = "Test content for plaintext\n"
        self.assertIsNotNone(self.parser.plain())

    def test_config_get_key_value(self):
        self.parser = Parser(self.tempfile)
        self.parser.fd = "Test content for key value\n"
        self.assertIsNotNone(self.parser.key_value())

    def test_config_get_complex(self):
        self.parser = Parser(self.tempfile)
        self.parser.fd = "ACCEPT loc net"
        self.assertIsNotNone(self.parser.complex())

        self.parser.fd = "ACCEPT loc net icmp"
        self.assertIsNotNone(self.parser.complex())

        self.parser.fd = "ACCEPT loc net tcp 80"
        self.assertIsNotNone(self.parser.complex())

        self.parser.fd = "ACCEPT loc net tcp 80 50000"
        self.assertIsNotNone(self.parser.complex())

        self.parser.fd = "ACCEPT loc:$HOST net:$HOST tcp 80 50000"
        self.assertIsNotNone(self.parser.complex())

        self.parser.fd = "ACCEPT loc:$HOST net:$HOST icmp"
        self.assertIsNotNone(self.parser.complex())

        self.parser.fd = "$MY) $src_HOST:$IP:$Port $dst_Host:$iP:$pOrt $proto $dport $sport"
        self.assertIsNotNone(self.parser.complex())

        self.parser.fd = "REDIRECT	loc:$HOST_RPI	3128	tcp	1024:2048,2050:3631,3633:40999,41003:65535"
        self.assertIsNotNone(self.parser.complex())

    def test_config_get_interfaces(self):
        self.parser = Parser(self.tempfile)
        self.parser.fd = "loc eth0 tcp,smurf"
        self.assertIsNotNone(self.parser.interfaces())


if __name__ == "__main__":
    unittest.main(verbosity=2)
