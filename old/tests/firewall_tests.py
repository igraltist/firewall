import os
import sys
import unittest
import tempfile


sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "firewall"))
print(sys.path)
import firewall


class FirewallTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, firewall.app.config["DATABASE"] = tempfile.mkstemp()
        dir(firewall)
        self.app = firewall.app.test_client()
        firewall.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(firewall.app.config["DATABASE"])

    def test_empty_db(self):
        rv = self.app.get("/")
        assert b"No entries here so far" in rv.data

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)


    def test_login_logout(self):
        rv = self.login('admin', 'default')
        assert 'You were logged in' in rv.data
        rv = self.logout()
        assert 'You were logged out' in rv.data
        rv = self.login('adminx', 'default')
        assert 'Invalid username' in rv.data
        rv = self.login('admin', 'defaultx')
        assert 'Invalid password' in rv.data

if __name__ == "__main__":
    unittest.main()
