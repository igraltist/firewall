import os
from threading import Lock
from datetime import datetime

from flask import (Flask, abort, g, render_template, request, redirect, 
    url_for, jsonify, session)
from flask_security import current_user, login_required

from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException

from firewall.views.dashboard import dashboard
from firewall.views.api import api 

from firewall.utils import (get_instance_folder_path, get_app_name,
    format_datetime, capitalize_words)
from firewall.cache import cache
from firewall.config import configure_app
from firewall.language import configure_language

from firewall.models import db

app = Flask(__name__,
    instance_path=get_instance_folder_path(),
    instance_relative_config=True,
    template_folder='templates',
    static_folder=None
)

configure_app(app)
configure_language(app)

app.static_folder = 'static'
uri = app.config.get("STATIC_URI", "/static")
subdomain = app.config.get("SUBDOMAIN", None)
app.add_url_rule("/".join([uri, '<path:filename>']),
     endpoint='static',
     subdomain=subdomain,
     view_func=app.send_static_file)


#cache.init_app(app)
db.init_app(app)

app.jinja_env.add_extension('jinja2.ext.loopcontrols')
app.jinja_env.filters['datetime'] = format_datetime
app.jinja_env.filters['capitalize_words'] = capitalize_words

def after_this_request(f):
    if not hasattr(g, 'after_request_callbacks'):
        g.after_request_callbacks = []
    g.after_request_callbacks.append(f)
    return f

@app.after_request
def call_after_request_callbacks(response):
    for callback in getattr(g, 'after_request_callbacks', ()):
        callback(response)
    return response

@app.before_request
def set_subdir_path():
    subdir = app.config.get("SUBDIR", None)
    if subdir:
        @after_this_request
        def set_subdir_cookie(response):
            response.set_cookie("subdir", subdir)

@app.before_first_request
def init_db():
    from firewall.models import User, Role
    db.create_all()
    admin = User.query.filter_by(username="admin").first() 
    role = Role.query.filter_by(name="admin").first()
    if role is None:
        role = Role(name="admin")
        db.session.add(role)
        db.session.commit()
    if admin is None:
        admin = User(username="admin", email="admin@localhost", 
                     password="admin", active=True, 
                     created=datetime.now(), roles=[role])
        db.session.add(admin)
        db.session.commit()


@app.before_request
def auth_needed():
    endpoints = ["security.login"]
    exclude_endpoints = ["static", None, "configmodification"]
    if request.endpoint in exclude_endpoints: 
        return 
    if (not current_user.is_authenticated  
        and request.endpoint not in endpoints): 
        return redirect(url_for("security.login")), 301

@app.errorhandler(404)
def page_not_found(error):
    app.logger.error('Page not found: %s', (request.path, error))
    return render_template('404.html', error=error), 404

@app.errorhandler(500)
def internal_server_error(error):
    app.logger.error('Server Error: %s', (error))
    return render_template('500.html'), 500

#@app.errorhandler(Exception)
#def unhandled_exception(error):
#    app.logger.error('Unhandled Exception: %s', (error))
#    return render_template('500.html'), 500

@app.context_processor
def inject_data():
    return dict(user=current_user, \
        lang_code=g.get('lang_code', None))

@app.route('/')
@app.route('/<lang_code>/')
#@cache.cached(300)
def home(lang_code=None):
    return redirect(url_for("dashboard.index"))

# register blueprints
app.register_blueprint(dashboard, url_prefix='/dashboard')
app.register_blueprint(dashboard, url_prefix='/<lang_code>/dashboard')
app.register_blueprint(api, url_prefix='/api')
app.register_blueprint(api, url_prefix='/<lang_code>/api')
