$(document).ready(function() {
    /***
     * copyright 2017, Jens Kasten
     */


    /***
     * Central place for selectors.
     * For convention the page are the prefix for selectors.
     * 
     *  id: 
     *      // where rules the convention of Firewall-Rules webpage
     *      selector.rules.my_selector
     *
     *  modal:
     *      selector.modal.my_selector
     *
     *  class:
     *      selector.my_class_selector
     *
     */
    var selector = {
        "input": {
            "text": "input[type=text]",
            "email": "input[type=email]",
            "password": "input[type=password]",
        },
        "rules": {

        },
        "configs": {

        },
        "modal": {
            "create": "#modal-create",
            "edit": "#modal-edit",
            "rename": "#modal-rename",
            "delete": "#modal-delete",
            "interfaces": "#modal-interfaces",
            "zones": "#modal-zones",
            "meminfo": "#modal-meminfo",
            "id": {
                "edit": "#edit-config",
                "config_name_create": "#config-name-create",
                "config_name_rename": "#config-name-rename",
                "config_name_delete": "#config-name-delete",
                "create": "#create-config",
                "save": "#save-config",
                "rename": "#rename-config",
                "delete": "#delete-config",
            },
        },
        "id": {
            "configs": "#configs",
            "rules": "#rules",

        },
        "info_box": ".info-box",
        "create_config": ".create-config",
        "reset_input":  ".reset-input",
        "edit_config": ".edit-config",
        "edit_rule": ".edit-rule",
        "zone": ".zone",
        "meminfo": ".meminfo",
        "modus": {
            "edit": "#modus-edit",
            "rename": "#modus-rename",
            "delete": "#modus-delete",
            "create": "#modus-create",
            "click": "#modus-click",
        },
        "action": {
            "click": "#action-click",
            "start": "#action-start",
            "stop": "#action-stop",
            "restart": "#action-restart",
            "clear": "#action-clear",
            "reload": "#action-reload",
            "check": "#action-check",
            "status": "#action-status",
        },
        "spinner": "#spinner",
    }

    /* actived the current link */
    $('a[href="' + this.location.pathname + '"]').parents('li,ul').addClass('active');

    /***
     * Resize the modal-body height to calculated size.
     * It must use the window hight insteed of content_height 
     * because a bug in jquery while resize to smaller value.
     */
    function setTextareaSize() {
        // offset is a guess value 
        var offset = 100;
        var window_height = $(window).height();
        var content_height = $(selector.modal.edit + " .modal-content").height();
        var header_height = $(selector.modal.edit + " .modal-header").height();
        var footer_height = $(selector.modal.edit + " .modal-footer").height();
        var body_height = window_height - header_height - footer_height - offset;

        // set the new height for modal-body
        $(selector.modal.edit + " .modal-body").css("height", body_height);
    }

    /***
     * Helper function to set info box
     */
    function setInfoBox(msg, color) {
        $(selector.info_box).html(msg);
        $(selector.info_box).addClass(color);
    }   

    function clearInfoBox() {
        $(selector.info_box).empty();
        $(selector.info_box).removeClass("alert");
        $(selector.info_box).removeClass("alert-danger");
        $(selector.info_box).removeClass("alert-success");
        $(selector.info_box).removeClass("alert-info");
    }

    function clearInput() {
        $(selector.input.text).val("");
    }

    /***
     * Show the modal for textarea
     */
    function displayTextarea() {
        clearInfoBox();
        $(selector.modal.edit + " textarea").val("");
        $(selector.modal.edit).css("display", "block");
        $(selector.modal.edit).modal('show');
        loadTextarea();
        setTextareaSize();
    }

    /**
     * retrieve data for meminfo
     */
    function loadMeminfo() {
        var request = $.ajax({
            url: Firewall.url, 
            type: 'GET',
            dataType: 'json',
        });

        request.fail(function(xhr, status, error) {
            if (xhr.hasOwnProperty("responseJSON")) {
                setInfoBox(xhr.responseJSON.message, "alert alert-danger");
            } else { 
                setInfoBox(error, "alert alert-danger");
            }
        });

        request.done(function(json, status, xhr) {
            if (json.data) {
                $(selector.modal.meminfo + " .read-from").append(json.data["read-from"]);
                for (key in json.data) {
                    // exclude the placeholder for the filename
                    if (key == "read-from") {
                        continue;
                    }
                    var tr = "<tr><td>" + key + "</td><td>" + json.data[key].value;
                    tr += "</td><td>" + json.data[key].unit + "</td></tr>";
                    $(selector.modal.meminfo + " table > tbody:last").append(tr);  
                }
            }  
            if (json.error) {
                setInfoBox(json.error, "alert alert-danger");
            }
        });
    }
    


    /**
     * retrieve data for textarea
     */
    function loadTextarea() {
        var request = $.ajax({
            url: Firewall.url, 
            type: 'GET',
            dataType: 'json',
        });

        request.fail(function(xhr, status, error) {
            if (xhr.hasOwnProperty("responseJSON")) {
                setInfoBox(xhr.responseJSON.message, "alert alert-danger");
            } else { 
                setInfoBox(error, "alert alert-danger");
            }
        });

        request.done(function(json, status, xhr) {
            if (json.data) {
                $(selector.modal.edit + " textarea").val(json["data"]);  
            }  
            if (json.error) {
                setInfoBox(json.error, "alert alert-danger");
            }
        });
    }
   
    /***
     * do action for shorewall
     */
    function doAction(action) {
        var request = $.ajax({
            url: Firewall.url, 
            type: 'POST',
            dataType: 'json',
        });

        clearInfoBox();

        // disable action button 
        $(selector.action.start).prop("disabled", true);
        $(selector.action.stop).prop("disabled", true);
        $(selector.action.restart).prop("disabled", true);
        $(selector.action.clear).prop("disabled", true);
        $(selector.action.check).prop("disabled", true);
        $(selector.action.reload).prop("disabled", true);
        $(selector.action.status).prop("disabled", true);

        $(selector.spinner).show();

        request.fail(function(xhr, status, error) {
            if (xhr.hasOwnProperty("responseJSON")) {
                setInfoBox(xhr.responseJSON.message, "alert alert-danger");
            } else { 
                setInfoBox(error, "alert alert-danger");
            }
        });

        request.done(function(json, status, xhr) {
            if (json.data) {
                setInfoBox(json.data, "alert alert-success");
            } else if (json.error) {
                setInfoBox(json.error, "alert alert-danger");
            }
        });

        request.always(function() {
            // enable action button
            $(selector.action.start).prop("disabled", false);
            $(selector.action.stop).prop("disabled", false);
            $(selector.action.restart).prop("disabled", false);
            $(selector.action.clear).prop("disabled", false);
            $(selector.action.check).prop("disabled", false);
            $(selector.action.reload).prop("disabled", false);
            $(selector.action.status).prop("disabled", false);

            $(selector.spinner).hide();
        });
    }

    /***
     * load configs 
     */
    function loadConfigs() {
        var request = $.ajax({
            url: Firewall.url, 
            type: 'GET',
            dataType: 'json',
        });

        request.fail(function(xhr, status, error) {
            if (xhr.hasOwnProperty("responseJSON")) {
                setInfoBox(xhr.responseJSON.message, "alert alert-danger");
            } else { 
                setInfoBox(error, "alert alert-danger");
            }
        });

        request.done(function(json, status, xhr) {
            if (json.data) {
                var i;
                for(i in json.data) {
                    var a = '<a href="#' + json.data[i] + '"';
                    a += ' data-file-id="' + json.data[i] +'"';
                    a += ' class="list-group-item">' + json.data[i] + '</a>';
                    $(selector.id.configs).append(a);  
                }
            }  
            if (json.error) {
                setInfoBox(json.error, "alert alert-danger");
            }
        });

    }

    /***
     * save data form 
     */
    function saveConfig() {
        // disable text input while transfering
        $(selector.modal.id.edit).prop("disabled", true);

        var request = $.ajax({
            url: Firewall.url,
            type: "PUT",
            dataType: "json",
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify({"data": Firewall.data}),
        });

        request.fail(function(xhr, status, error) {
            if (xhr.hasOwnProperty("responseJSON")) {
                setInfoBox(xhr.responseJSON.message, "alert alert-danger");
            } else { 
                setInfoBox(error, "alert alert-danger");
            }
        });

        request.done(function(json, status, xhr) {
            if (json.data) {
                setInfoBox(json.data.message, "alert alert-success");
                setTextareaSize();
            } 
            if (json.error) {
                setInfoBox(json.error, "alert alert-danger");
            }
        });

        request.always(function() {
            // enable text input when transfering ready
            $(selector.modal.id.edit).prop("disabled", false);
        });
    }

    /***
     * create default config file for rules
     */
    function createConfig() {
        var request = $.ajax({
            url: Firewall.url,
            type: 'POST',
            dataType: 'json',
        });

        request.fail(function(xhr, status, error) {
            if (xhr.hasOwnProperty("responseJSON")) {
                setInfoBox(xhr.responseJSON.message, "alert alert-danger");
            } else {
                setInfoBox(error, "alert alert-danger")
            }
        });

        request.done(function(json, status, xhr) {
            if (json.error) {
                setInfoBox(json.error, "alert alert-danger");
            } 
            if (json.data) {
                // set new ids
                Firewall.file_id = json.data.file_id;
                Firewall.zone_id = json.data.zone_id;

                // create a new link to append
                var data = 'data-zone-id="' + json.data.zone_id + '"';
                data += ' data-file-id="' + json.data.file_id + '"';
                data += ' data-hash-id="' + json.data.hash_id + '"';
                data += ' id="' + json.data.hash_id + '"';
                var new_a = '<a href="#" ' + data + '" class="list-group-item">';
                new_a += json.data.file_id + '</a>';
                
                // set selector
                var toggle = "#toggle-" + json.data.zone_id + "";
                
                // append new create link 
                $(toggle).append(new_a);
                
                // sort the div
                //var aSort = $(toggle + " a").sort(function(a, b) {
                 //   return $(a).text() > $(b).text() ? 1 : -1;
                //});

                // clean div and append new sorted content
                //$(toggle).empty().html(aSort)
                
                // sort div
                sortNames(toggle, "a")

                // update zone counter 
                $("#count-" + json.data.zone_id).text($(toggle + " > a").length);
                
                // hide all accordion div
                $(".collapse").collapse("hide");
                // hide the modal dialog 
                $(selector.modal.create).modal("hide");

                var result = function() {
                    $(toggle).collapse("show");
                    $("html, body").animate({
                        scrollTop: $("#" + json.data.hash_id).offset().top
                    }, 1000);
                    Firewall.set_url();
                    Firewall.set_title("edit");
                    displayTextarea();
                };
                var wait = function() {
                   var deferred = $.Deferred();
                   setTimeout(function() {
                        deferred.resolve();
                   }, 500);
                        
                   return deferred.promise();
                };
                var promise = wait();
                promise.done(result);
            } 
            request.always(function() {
            });
        });
    }

    /***
     * rename config file 
     */
    function renameConfig() {
        var request = $.ajax({
            url: Firewall.url,
            type: 'PUT',
            dataType: 'json',
        });

        request.fail(function(xhr, status, error) {
            if (xhr.hasOwnProperty("responseJSON")) {
                setInfoBox(xhr.responseJSON.message, "alert alert-danger");
            } else { 
                setInfoBox(error, "alert alert-danger");
            }
        });

        request.done(function(json, status, xhr) {
            if (json.error) {
                setInfoBox(json.error, "alert alert-danger");
            } 
            if (json.data) {
                // this part has to clean up
                Firewall.file_id = json.data.new_file_id;
                Firewall.zone_id = json.data.zone_id

                var toggle = "#toggle-" + json.data.zone_id + "";
                // create a new link to append
                var data = 'data-zone-id="' + json.data.zone_id + '"';
                data += ' data-file-id="' + json.data.file_id + '"';
                data += ' data-hash-id="' + json.data.hash_id + '"';
                data += ' id="' + json.data.hash_id + '"';
                var new_a = '<a href="#" ' + data + '" class="list-group-item">';
                new_a += json.data.file_id + '</a>';
    
                // append new link 
                $(toggle).append(new_a);
                // remove old link
                $(toggle + " a").remove("#" + Firewall.hash_id);

                // hide all accordion div
                $(".collapse").collapse("hide");
                // hide the modal dialog 
                $(selector.modal.rename).modal("hide");

                var result = function() {
                   // return
                    $(toggle).collapse("show");
                    sortNames(toggle, "a");
                    return
                    $("html, body").animate({
                        scrollTop: $("#" + json.data.hash_id).offset().top
                    }, 1000);
                };
                var wait = function() {
                   var deferred = $.Deferred();
                   setTimeout(function() {
                        deferred.resolve();
                   }, 500);
                        
                   return deferred.promise();
                };
                var promise = wait();
                promise.done(result);
            } 
        
        });
        request.always(function() {
        });

    }
    /***
     * delete config file 
     */
    function deleteConfig() {
        var request = $.ajax({
            url: Firewall.url,
            type: 'DELETE',
            dataType: 'json',
        });

        request.fail(function(xhr, status, error) {
            if (xhr.hasOwnProperty("responseJSON")) {
                setInfoBox(xhr.responseJSON.message, "alert alert-danger");
            } else { 
                setInfoBox(error, "alert alert-danger");
            }
        });

        request.done(function(json, status, xhr) {
            if (json.error) {
                setInfoBox(json.error, "alert alert-danger");
            } 
            if (json.data) {
                // this part has to clean up
                Firewall.file_id = json.data.new_file_id;
                Firewall.zone_id = json.data.zone_id

                var toggle = "#toggle-" + json.data.zone_id;
                // remove old link
                $(toggle + " a").remove("#" + Firewall.hash_id);
                
                // update zone counter 
                $("#count-" + json.data.zone_id).text($(toggle + " > a").length);

                 // hide all accordion div
                $(".collapse").collapse("hide");
                // hide the modal dialog 
                $(selector.modal.delete).modal("hide");

                var result = function() {
                   // return
                    $(toggle).collapse("show");
                    sortNames(toggle, "a");
                };
                var wait = function() {
                   var deferred = $.Deferred();
                   setTimeout(function() {
                        deferred.resolve();
                   }, 500);
                        
                   return deferred.promise();
                };
                var promise = wait();
                promise.done(result);
            } 
        
        });
        request.always(function() {
        });

    }

    /***
     * sort function for a div 
     */
    function sortNames(id, target) {
        var aSort = $(id + " " + target).sort(function(a, b) {
            var at = $(a).text().toUpperCase();
            var bt = $(b).text().toUpperCase();

            return (at < bt) ? -1 : (at > bt) ? 1 : 0;
        });
        $(id).empty().html(aSort)
    }

    /***
     * Helper function to return a string insteed of undefined, false, or null..
     */
    function has_value(value) {
        if (value)
            return value 
        else 
            return "";
    }

    /***
     * exctact a cookie by give name
     */
    function get_cookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        var value;
        
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                value = c.substring(name.length, c.length);
                return value.replace(/([^a-z0-9\/])/gi, "");
            }
        }
        return "";
    }

    /***
     * class to controll the flow 
     */
    var Firewall = {
        api_version: "1.0",
        api_config: {},
        cookie: null,
        zone_id: null,
        file_id: null,
        hash_id: null,
        hash_id_old: null,
        title: null,
        url: null,
        url_old: null,
        data: null,
        modi: {
            edit: true,
            create: false,
            delete: false,
            rename: false,
        },
        modus: "edit",
        parentNode: null,
        event: null,
        title: {
            edit: null,
            create: null,
            rename: null,
            delete: null,
        },
        view: null,
       
        /***
         * Safe values which later override.
         */ 
        init: function() {
            // set default modus for a rule
            $(selector.modus.edit).addClass("active");
            $(selector.spinner).hide();
        
            // save default modal titles
            Firewall.title.edit = $(selector.modal.edit + " .modal-title").text();
            Firewall.title.create = $(selector.modal.create + " .modal-title").text();
            Firewall.title.rename = $(selector.modal.rename + " .modal-title").text();
            Firewall.title.delete = $(selector.modal.delete + " .modal-title").text();
        },
        set_title: function(name) {
            var title = Firewall.title[name]
            
            if (Firewall.file_id) {
                title += Firewall.file_id;
            }
            if (Firewall.file_id && Firewall.zone_id) {
                title += " in zone: " + Firewall.zone_id;
            } 
            if (Firewall.file_id == null) {
                title += Firewall.zone_id;
            }

            $(selector.modal[name] + " .modal-title").empty();
            $(selector.modal[name] + " .modal-title").text(title);
        },
        /***
         * Set modus for rule
         */
        set_modus: function(modus) {
            for(name in Firewall.modi) {
                if (name == modus) {
                    $(selector.modus[name]).addClass("active");
                    Firewall.modi[name] = true;
                    Firewall.modus = modus;
                } else {
                    $(selector.modus[name]).removeClass("active");
                    Firewall.modi[name] = false;
                }
            }
        },
        set_ids: function(event) {
            Firewall.event = event;
            Firewall.parentNode = event.target.parentNode.id;
            Firewall.hash_id = null;
            Firewall.file_id = null;
            Firewall.zone_id = null;
        
            event.preventDefault();
            event.stopPropagation();
            
            if (event.target.attributes["data-hash-id"]) {
                Firewall.hash_id = event.target.attributes["data-hash-id"].value;
            }
            if (event.target.attributes["data-file-id"]) {
                Firewall.file_id = event.target.attributes["data-file-id"].value;
            }
            if (event.target.attributes["data-zone-id"]) {
                Firewall.zone_id = event.target.attributes["data-zone-id"].value;
            }

        },
        /***
         * Build url for api depend on view.
         */
        set_url: function() {
            // reset the url to default for current view
            Firewall.url = Firewall.api_config[Firewall.view];

            switch(Firewall.view) {
                case "rule": 
                    Firewall.url += Firewall.zone_id + "/";
                    if (Firewall.file_id) {
                        Firewall.url += Firewall.file_id;
                    }
                    break;
                default:
                    if (Firewall.file_id) {
                        Firewall.url += Firewall.file_id;
                    }
            }
        },
        /***
         * Set default api version.
         */
        set_api: function(api_version) {
            // always can be overwritten the default api verions via arg
            if (!api_version) {
                api_version = Firewall.api_version;
            }
            
            var subdir = get_cookie("subdir");

            Firewall.api_config = {
                "rule": subdir + "/api/" + api_version + "/rule/",
                "config": subdir + "/api/" + api_version + "/config/",
                "setting": subdir + "/api/" + api_version + "/setting/",
                "system": subdir + "/api/" + api_version + "/system/",
                "action": subdir + "/api/" + api_version + "/action/",
                "info": subdir + "/api/" + api_version + "/info/",
            }
        },
        /***
         * Set the view depend on pathname.
         * All api paths are depend from view.
         */
        set_route: function() {
            // get uri name
            var pathname = window.location.pathname; 

            Firewall.view = "default";

            if (pathname.match("Configs")) {
                Firewall.view = "config";
            } else if (pathname.match("Rules")) {
                Firewall.view = "rule";
            } else if (pathname.match("Settings")) {
                Firewall.view = "setting";
            } else if (pathname.match("Action")) {
                Firewall.view = "action";
            } else if (pathname.match("Info")) {
                Firewall.view = "info";
            }

            // set default api
            Firewall.set_api();

            // set base url depends on view
            Firewall.url = Firewall.api_config[Firewall.view];
        },
        /***
         * A function that preload values depend on view without an event.
         */
        run: function() {
            switch(Firewall.view) {
                case "config": 
                    loadConfigs();
                    break;
                default:
                    break;
            }
        }
    };

    // init classes
    Firewall.init();
    Firewall.set_route();
    Firewall.run()

    /***
     * Workaround to clear then infobox after the content is saved 
     * and after that again the content is modified.
     */
    $(selector.modal.id.edit).keydown(function() {
        clearInfoBox();
    });

    // clicked on id #action-click
    $(selector.action.click + " button").click(function(event) {
        var action = event.target.id.split("-")[1];
        Firewall.url = Firewall.api_config["action"] + action;

        doAction(action);
    });

    // clicked on id #modus-click
    $(selector.modus.click + " button").click(function(event) {
        // get id from button and set modus, id is like modus-edit or modus-delete
        Firewall.set_modus(event.target.id.split("-")[1]);
    }); 

    // clicked on page link class .edit-config
    $(selector.edit_config).on('click', 'a', function(event) {
        Firewall.set_ids(event);
        Firewall.set_url();
        Firewall.url_old = Firewall.url;
        clearInfoBox();

        switch (Firewall.modus) {
            case "edit": 
                Firewall.set_title("edit")
                displayTextarea();
                break;
            case "delete":
                Firewall.set_modus("edit");
		Firewall.set_title("delete");
                
		$(selector.modal.delete + " " + selector.input.text).val(Firewall.file_id);
                $(selector.modal.delete).modal("show");
                break;
            case "rename": 
                Firewall.set_modus("edit");
                Firewall.set_title("rename");
                // split and then remove suffix after last .
                var file_id = Firewall.file_id.split(".")
                file_id = Firewall.file_id.replace("." + file_id.pop(), "")

                $(selector.modal.rename + " " + selector.input.text).val(file_id);
                $(selector.modal.rename).modal("show");
                break;
        }
    });

    // clicked on modal to rename the config file
    $(selector.modal.id.rename).click(function() {
        // append the new filename
        var new_file_id = $(selector.modal.rename + " " + selector.input.text).val();

        Firewall.url = Firewall.url_old;
        Firewall.url += "/" + new_file_id;

        console.log(Firewall.url)
        if (new_file_id.length >= 2) {
            Firewall.file_id = new_file_id;
            renameConfig();
        } else {
            setInfoBox("Length need a least two.", "alert alert-danger");
        }
    });

    // clicked on modal to delete the config file
    $(selector.modal.id.delete).click(function() {
        deleteConfig();
    });

    // clicked on modal to save the new config file
    $(selector.modal.id.save).click(function() {
        Firewall.data = $(selector.modal.id.edit).val();
        saveConfig();
    });

    // clicked on page dropdown create to select a zone
    $(selector.create_config).click(function(event) {
        clearInfoBox();
        Firewall.set_ids(event);
        Firewall.set_title("create");
        Firewall.set_modus("edit");
        clearInfoBox();
        $(selector.modal.create).modal("show");
    });

    //  clicked on modal create config 
    $(selector.modal.id.create).click(function() {
        Firewall.file_id = $(selector.input.text).val();
        Firewall.set_url()
        createConfig();
    });

    // clicked dashboard home meminfo
    $(selector.meminfo).click(function(event) {
        Firewall.url = Firewall.api_config["system"] + "meminfo";
        clearInfoBox();
        $(selector.modal.meminfo).modal("show");
        loadMeminfo();
    });

    // clicked on modal reset
    $(selector.reset_input).click(function(event) {
        clearInfoBox();
        clearInput();
    });

    // when window resize the height of modal-body
    $(window).resize(function(event) {
        setTextareaSize();
    });
});
