import os
import logging

from flask_compress import Compress
from flask_security import Security

from firewall.utils import get_app_name, get_app_base_path
from firewall.form_login import ExtendedLoginForm, user_datastore

# 
email_format = '''
Message type:       %(levelname)s
Location:           %(pathname)s:%(lineno)d
Module:             %(module)s
Function:           %(funcName)s
Time:               %(asctime)s

Message:

%(message)s
'''

class BaseConfig(object):
    APPLICATION_ROOT = None 
    DEBUG = False
    TESTING = False

    # default config loading via CONFIG_NAME
    CONFIG_TYPE = "production"
    # default subdir is empty
    SUBDIR = ""

    # database
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SECRET_KEY = 'BTMmjXOkt7NvogaJ50AD=8VefsZ3$wW/'

    # logging
    LOGGING_FORMAT = "%(asctime)s | %(pathname)s:%(lineno)d | %(funcName)s | %(levelname)s | %(message)s "
    LOGGING_FILE_NAME = ".".join([get_app_name(), "log"])
    LOGGING_LOCATION = os.path.join(get_app_base_path(), "log", LOGGING_FILE_NAME)

    # email
    EMAIL_USE = False
    EMAIL_ADMINS = ["root@localhost"]
    EMAIL_SERVER = "127.0.0.1"
    EMAIL_SENDER = "root@localhost"
    EMAIL_SUBJECT = "Application ERROR"

    # cache
    CACHE_TYPE = 'simple'
    COMPRESS_MIMETYPES = ['text/html', 'text/css', 'text/xml', 
        'application/json', 'application/javascript']
    COMPRESS_LEVEL = 6
    COMPRESS_MIN_SIZE = 500

    # translation 
    SUPPORTED_LANGUAGES = {'de': 'German', 'en': 'English'}
    BABEL_DEFAULT_LOCALE = 'de'
    BABEL_DEFAULT_TIMEZONE = 'UTC'

    # security 
    SECURITY_CONFIRMABLE = False
    SECURITY_USER_IDENTITY_ATTRIBUTES = 'username'
    SECURITY_POST_LOGIN_VIEW = "dashboard.index"
    SECURITY_POST_LOGOUT_VIEW = ""
    # url prefix has to set when the application is like mydomain/firewall
    SECURITY_URL_PREFIX = None
    
    # shorewall 
    SHOREWALL_LOCATION = "/etc/shorewall"
    SHOREWALL_LOCATION_RULES = "rules.d"
    SHOREWALL_RULES_SUFFIX = ".rules"
    SHOREWALL_EXCLUDE_ENDSWITH = [".swp", ".annotated", "._"]
    SHOREWALL_EXCLUDE_CONFIGS = ["zones", "interfaces", "Makefile"]


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:////%s/%s/%s.db' % (get_app_base_path(),
        "database", get_app_name())
    SHOREWALL_LOCATION = os.path.join(get_app_base_path(), "shorewall")
    TEMPLATES_AUTO_RELOAD = True
    LOGGING_LEVEL = logging.DEBUG


class TestingConfig(BaseConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:////%s/%s/%s.db' % (get_app_base_path(),
        "database", get_app_name())
    SHOREWALL_LOCATION = os.path.join(get_app_base_path(), "shorewall")
    EXPLAIN_TEMPLATE_LOADING = True
    LOGGING_LEVEL = logging.DEBUG


class ProductionConfig(BaseConfig):
    LOGGING_LEVEL = logging.INFO
    # just in case disabled if environ somewhere set it
    DEBUG = False
    TESTING = False

config = {
    "development": "firewall.config.DevelopmentConfig",
    "testing": "firewall.config.TestingConfig",
    "production": "firewall.config.ProductionConfig",
}

def configure_app(app):
    # read instanc config file and prepearing
    filename = os.path.join(app.instance_path, "application.cfg")
    app_config = {}

    if os.path.isfile(filename):
        with open(filename) as fd:
            for line in fd.readlines():
                line = line.strip()
                if line.startswith("#"):
                    continue
                temp = line.split("=")
                if len(temp) == 1:
                    continue
                key = temp[0].strip()
                value = temp[1].strip()
                app_config[key] = value

    # choose config based on CONFIG_NAME
    config_name = app_config.get("CONFIG_TYPE", BaseConfig.CONFIG_TYPE)
    # update app config
    app.config.from_object(config[config_name])
    # override config from application.cfg
    app.config.update(app_config)
   
    # Configure logging
    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)

    if app.config["CONFIG_TYPE"] == "development":
        # log lower level
        log = logging.getLogger('werkzeug')
        app.logger.addHandler(log)

    if app.config["EMAIL_USE"]:
        from logging.handlers import SMTPHandler
        mail_handler = SMTPHandler(
            app.config["EMAIL_SERVER"],
            app.config["EMAIL_SENDER"],
            app.config["EMAIL_ADMINS"],
            app.config["EMAIL_SUBJECT"])
        mail_handler.setLevel(logging.ERROR)
        mail_handler.setFormatter(logging.Formatter(email_format))
        app.logger.addHandler(mail_handler)

    # Configure Security
    app.security = Security(app, user_datastore, login_form=ExtendedLoginForm)
    # Configure Compressing
    Compress(app)
