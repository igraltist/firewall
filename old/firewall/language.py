from flask_babel import Babel
from flask import abort, g, redirect, request

def configure_language(app):
    babel = Babel(app)

    @babel.localeselector
    def get_locale():
        lang = request.args.get("lang")
        if lang is None:
            return g.get('lang_code', app.config['BABEL_DEFAULT_LOCALE'])
        elif lang not in app.config["SUPPORTED_LANGUAGES"]:
            return g.get('lang_code', app.config['BABEL_DEFAULT_LOCALE'])
        else:
            g.lang_code = lang
            return lang

    @babel.timezoneselector
    def get_timezone():
        user = g.get('user', None)
        if user is not None:
            return user.timezone

    @app.before_request
    def ensure_lang_support():
        lang_code = g.get('lang_code', None)
        if lang_code and lang_code not in app.config['SUPPORTED_LANGUAGES'].keys():
            g.lang_code = app.config["BABEL_DEFAULT_LOCALE"]
            return abort(404)

    @app.url_defaults
    def set_language_code(endpoint, values):
        if 'lang_code' in values or not g.get('lang_code', 
                app.config['BABEL_DEFAULT_LOCALE']):
            return
        if app.url_map.is_endpoint_expecting(endpoint, 'lang_code'):
            values['lang_code'] = g.get('lang_code',
                app.config['BABEL_DEFAULT_LOCALE'])
            #values['lang_code'] = g.lang_code

    @app.url_value_preprocessor
    def get_lang_code(endpoint, values):
        if values is not None:
            g.lang_code = values.pop('lang_code', app.config['BABEL_DEFAULT_LOCALE'])


