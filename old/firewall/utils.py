import os
from babel import dates

def get_app_base_path():
    return os.path.dirname(os.path.realpath(__file__))

def get_instance_folder_path():
    fwconfig_dir = os.path.join(os.environ["HOME"], "fwconfig")
    if os.path.isdir(fwconfig_dir):
        return fwconfig_dir
    else:
        return os.path.join(get_app_base_path(), 'instance')

def get_app_name():
    return get_app_base_path().split("/")[-1]

def format_datetime(value, format='medium'):
    return dates.format_datetime(value, format)

def capitalize_words(value, split_by=" "):
    result = value.split(split_by)
    result = split_by.join([i.capitalize() for i in result])
    return result

