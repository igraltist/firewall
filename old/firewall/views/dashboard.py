from flask import Blueprint, render_template
from flask_security import current_user

from firewall.dashboard.system import uptime, meminfo
from firewall.dashboard.shorewall import (get_rules, get_config_files, 
        get_config)

dashboard = Blueprint('dashboard', 
                      __name__,
                      template_folder="template",
                      static_folder=None)


@dashboard.route('/')
def index():
    print(current_user.last_login)
    data = {
        "last_login": current_user.last_login,
        "uptime": uptime(),
        "meminfo": meminfo(),
    }
    return render_template("dashboard/index.html", data=data)

@dashboard.route('/Configs', methods=["GET"])
def firewall_configs():
    data = get_config_files()
    return render_template("dashboard/firewall-configs.html")

@dashboard.route('/Rules', methods=["GET"])
def firewall_rules():
    data = get_rules()
    return render_template("dashboard/firewall-rules.html", data=data)

@dashboard.route('/Settings', methods=["GET"])
def firewall_settings():
    data = {
        "interfaces": get_config("interfaces"),
        "zones": get_config("zones"),
    }
    return render_template("dashboard/firewall-settings.html", data=data)

@dashboard.route('/Action', methods=["GET"])
def firewall_action():
    return render_template("dashboard/firewall-action.html")

@dashboard.route('/Info', methods=["GET"])
def firewall_info():
    return render_template("dashboard/firewall-info.html")

