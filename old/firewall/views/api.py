from flask import Blueprint, jsonify, request, abort

from firewall.dashboard.shorewall import get_config_data, get_config_files
from firewall.dashboard.shorewall import (get_rule_data, create_rule_config, 
        save_rule_config, save_config_data, rename_rule_config, 
        delete_rule_config)
from firewall.dashboard.system import meminfo, do_action


# create a blueprint
api = Blueprint('api', __name__)

api_version = "1.0"

# creata a dict for api specific error messages to return in json format
errors = {
    400: {
        "method": "BAD REQUEST",
        "message": "Bad Request",
        "status": 400,
    },
    405: { 
        "method": "METHOD NO ALLOWED",
        "message": "The method is not allowed for the requested URL.",
        "status": 405,
    },
    404: {
        "method": "NOT Found",
        "message": "Not found",
        "status": 404,
    },
    500: {
        "method": "INTERAL SERVER ERROR",
        "message": "Internal server error occurs",
        "status": 500,
    },
}

@api.app_errorhandler(400)
def error_bad_request(error):
    return jsonify(errors[400]), 400

@api.app_errorhandler(405)
def error_method_not_allowed(error):
    return jsonify(errors[405]), 405

@api.app_errorhandler(404)
def error_not_found(error):
    return jsonify(errors[404]), 404

@api.app_errorhandler(401)
def error_forbidden(error):
    return jsonify(errors[401]), 401

@api.app_errorhandler(500)
def error_internal_error(error):
    return jsonify(errors[500]), 500


### start public api_version 1.0 ###
## config ##
@api.route("/%s/config/" % api_version, methods=["GET"])
def api_get_config_files():
    return jsonify(get_config_files())

@api.route("/%s/config/<string:file_id>" % api_version, methods=["GET"])
def api_get_config_data(file_id):
    return jsonify(get_config_data(file_id))

@api.route("/1.0/config/<string:file_id>", methods=["PUT"])
def api_save_config_data(file_id):
    if request.json:
        return jsonify(save_config_data(file_id, request.json)), 201
    else:
        return error_bad_request(400)

## rule ##
#@api.route("/%s/rule/" % api_version, methods=["GET"])
#def api_get_rules():
#    return jsonify(get_rules());

@api.route("/1.0/rule/<string:zone_id>/<string:file_id>", methods=["GET"])
def api_get_rule_data(zone_id, file_id):
    return jsonify(get_rule_data(zone_id, file_id))

@api.route("/1.0/rule/<string:zone_id>/<string:file_id>", methods=["POST"])
def api_create_rule_config(zone_id, file_id):
    return jsonify(create_rule_config(zone_id, file_id)), 201

@api.route("/1.0/rule/<string:zone_id>/<string:file_id>", methods=["DELETE"])
def api_delete_rule_config(zone_id, file_id):
    return jsonify(delete_rule_config(zone_id, file_id)), 201

@api.route("/1.0/rule/<string:zone_id>/<string:file_id>/<string:new_file_id>", methods=["PUT"])
def api_rename_rule_config(zone_id, file_id, new_file_id):
    return jsonify(rename_rule_config(zone_id, file_id, new_file_id)), 201

@api.route("/1.0/rule/<string:zone_id>/<string:file_id>", methods=["PUT"])
def api_save_rule_config(zone_id, file_id):
    if request.json:
        return jsonify(save_rule_config(zone_id, file_id, request.json)), 201
    else:
        return error_bad_request(400)


## setting ##

## system ##
@api.route("/1.0/system/meminfo", methods=["GET"])
def api_system_meminfo():
    return jsonify(meminfo())

## action ##
@api.route("/1.0/action/<string:action_id>", methods=["POST"])
def api_action(action_id):
    # in static/js/firewall.js do_action() add too to disabeled buttons
    allowed_action = ["start", "stop", "restart", "clear", "reload", "check",
        "status"]
    if action_id in allowed_action:
        return jsonify(do_action(action_id))
    else:
        return error_bad_request(400)


