from datetime import datetime 

from flask_security.forms import LoginForm, get_form_field_label, _default_field_labels
from flask_security.confirmable import requires_confirmation
from flask_security.utils import verify_and_update_password, get_message
from flask_security import SQLAlchemyUserDatastore

from wtforms import StringField
from wtforms.validators import InputRequired

from firewall.models import db, Role, User

# add new label field
_default_field_labels['username'] = 'Username'

#  
user_datastore = SQLAlchemyUserDatastore(db, User, Role)

# override the validate method 
class ExtendedLoginForm(LoginForm):
    username  = StringField(get_form_field_label('username'), [InputRequired()])
        
    def validate(self):
        if not super(LoginForm, self).validate():
            return False

        if self.username.data.strip() == '':
            self.username.errors.append(get_message('USERNAME_NOT_PROVIDED')[0])
            return False

        if self.password.data.strip() == '':
            self.password.errors.append(get_message('PASSWORD_NOT_PROVIDED')[0])
            return False

        self.user = user_datastore.get_user(self.username.data)

        if self.user is None:
            self.username.errors.append(get_message('USER_DOES_NOT_EXIST')[0])
            return False
        if not self.user.password:
            self.password.errors.append(get_message('PASSWORD_NOT_SET')[0])
            return False
        if not verify_and_update_password(self.password.data, self.user):
            self.password.errors.append(get_message('INVALID_PASSWORD')[0])
            return False
        if requires_confirmation(self.user):
            self.username.errors.append(get_message('CONFIRMATION_REQUIRED')[0])
            return False
        if not self.user.is_active:
            self.username.errors.append(get_message('DISABLED_ACCOUNT')[0])
            return False
        
        # update last_login
        self.user.last_login = datetime.now()
        db.session.add(self.user)
        db.session.commit()

        return True

