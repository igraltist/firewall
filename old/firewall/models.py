from flask_sqlalchemy import SQLAlchemy
from flask_security import UserMixin, RoleMixin

db = SQLAlchemy()


roles_users = db.Table('roles_users', 
                       db.Column('user_id', db.Integer(), db.ForeignKey('users.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('roles.id')))


class Manpages(db.Model):
    __tablename__ = "manpages"

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    available = db.Column(db.Boolean())
    created = db.Column(db.DateTime())
    last_update = db.Column(db.DateTime())

    def __init__(self, name, created):
        self.name = name
        self.created = created

    def __repr__(self):
        return "<Manpages %r>" % (self.name)


class Role(db.Model, RoleMixin):
    __tablename__ = "roles"

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Role %r>' % (self.name)


class User(db.Model, UserMixin):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    last_login = db.Column(db.DateTime())
    created = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __init__(self, username, email, password, active, created, roles):
        self.username = username
        self.email = email
        self.password = password
        self.active = active
        self.created = created
        self.roles = roles

    def __repr__(self):
        return '<User %r>' % (self.username)

