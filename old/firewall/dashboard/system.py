from datetime import timedelta
from subprocess import Popen, PIPE

from flask import current_app


def generate_manpages():
    """man -k '' | while read sLine; do
        declare sName=$(echo $sLine | cut -d' ' -f1)
        declare sSection=$(echo $sLine | cut -d')' -f1|cut -d'(' -f2)
        echo "converting ${sName}(${sSection}) to ${sName}.${sSection}.html ..."
        man -Thtml ${sSection} ${sName} > ${sName}.${sSection}.html
    done
    """


def disk_usage():
    pass

def meminfo():
    result = {
        "error": None,
        "data": {},
    }

    try: 
        with open("/proc/meminfo", "r") as fd:
            result["data"]["read-from"] = "/proc/meminfo"

            for line in fd.readlines():
                line = line.strip().split(":")
                if len(line) == 0:
                    continue
                key = line[0]
                line = line[1].lstrip().split(" ")
                value = line[0]
                if len(line) == 2:
                    unit = line[1]
                else: 
                    unit = ""
                result["data"][key] = {
                    "value": value,
                    "unit": unit,
                }
    except IOError as error:
        result["error"] = str(error)

    return result
            

def uptime():
    result = {
        "error": None,
        "data": None,
    }
    
    try:
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            uptime_string = str(timedelta(seconds=int(round(uptime_seconds))))
            result["data"] = uptime_string
    except IOError as error:
        result["error"] = str(error)

    return result

def make_paragraph(data):
    data = data.split("\n")
    temp = ""
    if len(data) == 2:
        temp = "<p>" + data[0] + "</p>"
    else:
        for i in data[:-2]:
            temp += "<p>" + i + "</p>"
    return temp


def do_action(action):
    error = None
    status = None
    result = {
        "error": error,
        "data": status,
    }

    try:
        if current_app.config.get("USE_SUDO", False) == "True":
            cmd = ["sudo", "shorewall", action]
        else:
            cmd = ["shorewall", action]
        process = Popen(cmd, stdout=PIPE, stderr=PIPE)
        status, error = process.communicate(timeout=15)
    except OSError as error_msg:
        result["error"] = str(error_msg)
        return result
    except TimeoutExpired:
        process.kill()
        status, error = process.communicate()

    if error:
        error = error.decode("utf-8")
        result["error"] = make_paragraph(error)
    if status:
        status = status.decode("utf-8")
        result["data"] = make_paragraph(status)
    return result
