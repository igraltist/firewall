import os
import re
import hashlib
from datetime import datetime

from shutil import copyfile
from flask import current_app

from firewall.dashboard.config_parser import Parser
from firewall.dashboard.utils import Placeholders, Manpages

from firewall.models import db, Manpages


def get_config_dir():
    data = {
        "config_dir": current_app.config["SHOREWALL_LOCATION"],
        "error": None,
    }
    if not os.path.isdir(data["config_dir"]):
        data["error"] = "No such directory: %s" % data["config_dir"]
    return data

def get_rules_dir():
    rules_dir = os.path.join(current_app.config["SHOREWALL_LOCATION"],
        current_app.config["SHOREWALL_LOCATION_RULES"])
    data = {
        "rules_dir": rules_dir,
        "error": None,
    }
    if not os.path.isdir(data["rules_dir"]):
        data["error"] = "No such directory %s" % data["rules_dir"]
    return data

def hash_id(zone_id, file_id):
    hash_id = zone_id + file_id
    return hashlib.md5(hash_id.encode("utf-8")).hexdigest()

def check_input(*args):
    print(args)

def build_manpages():
    manpages = {}
    #update = Manpages(name=name, created=datetime.now()) 
    return manpages

def get_config_files():
    data = {
        "error": None,
        "data": [],
    }
   
    config_dir = get_config_dir()
    if config_dir["error"]:
        data["error"] = config_dir["error"]
        return data
    config_dir = config_dir["config_dir"]

    manpages = Manpages.query.get("name")
    if manpages is None:
        manpages = build_manpages()
    
    for i in os.listdir(config_dir):
        # exclude e.g. '.swp'
        exclude = current_app.config["SHOREWALL_EXCLUDE_ENDSWITH"]
        exit = False
        for j in exclude:
            if i.endswith(j):
                exit = True
                continue
        if exit:
            continue
        elif os.path.isdir(os.path.join(config_dir, i)):
            continue
        elif not os.path.isfile(os.path.join(config_dir, i)):
            continue
        # exclude e.g interfaces or zones
        elif not i in current_app.config["SHOREWALL_EXCLUDE_CONFIGS"]:
            print(current_app.config["SHOREWALL_EXCLUDE_CONFIGS"], i)
            data["data"].append(i) 
    data["data"].sort()
    return data

def get_config_data(file_id):
    data = {
        "error": None,
        "data": [],
    }
    
    config_dir = get_config_dir()
    if config_dir["error"]:
        data["error"] = config_dir["error"]
        return data
    config_dir = config_dir["config_dir"]

    try: 
        config = os.path.join(config_dir, file_id)

        if os.path.isdir(config):
            data["error"] = "Config is a directory."
        elif not os.path.isfile(config):
            data["error"] = "Config does not exists."
        else:
            parser = Parser(config)
            data["data"] = parser.plain()
    except OSError as error:
        data["error"] = str(error)

    return data

def get_rules():
    zones = {}
    data = {
        "error": None,
        "data": zones,
    }

    rules_dir = get_rules_dir()
    if rules_dir["error"]:
        data["error"] = rules_dir["error"]
        return data
    rules_dir = rules_dir["rules_dir"]

    try:
        for zone in os.listdir(rules_dir):
            if os.path.isdir(os.path.join(rules_dir, zone)):
                zones[zone] = [];
                for config in os.listdir(os.path.join(rules_dir, zone)):
                    if config.endswith(current_app.config["SHOREWALL_RULES_SUFFIX"]):
                        zones[zone].append((config, hash_id(zone, config)))
                data["data"] = zones
    except OSError as error:
        data["error"] = str(error)

    return data

def get_rule_data(zone_id, file_id):
    result = {
        "error": None,
        "data": [],
    }

    rules_dir = get_rules_dir()
    if rules_dir["error"]:
        result["error"] = rules_dir["error"]
        return data

    rules_dir = rules_dir["rules_dir"]
    config = os.path.join(rules_dir, zone_id, file_id)

    if os.path.isdir(config):
        result["errro"] = "Config is a directory."
    elif not os.path.isfile(config):
        result["error"] = "Config does not exists."
    else:
        parser = Parser(config)
        result["data"] = parser.plain()

    return result
    
def create_rule_config(zone_id, file_id):
    result = {
        "error": None,
        "data": None,
    }

    rules_dir = get_rules_dir()
    if rules_dir["error"]:
        result["error"] = rules_dir["error"]
        return data
    rules_dir = rules_dir["rules_dir"]
    file_id = "".join([file_id, current_app.config["SHOREWALL_RULES_SUFFIX"]])
    config = os.path.join(rules_dir, zone_id, file_id)
    header = " ".join([i["name"].upper().replace("_","|") for i in Placeholders.rules().values()])
    header = "#" + header

    if os.path.isdir(config):
        result["error"] = "Config is a directory."
    elif os.path.isfile(config):
        result["error"] = "Config exists."
    elif file_id is None:
        result["error"] = "Data must not None."
    else:

        with open(config, "w") as fd:
            fd.write(header)
            fd.write("\n")
            result["data"] = {
                "zone_id": zone_id,
                "file_id": file_id,
                "hash_id": hash_id(zone_id, file_id),
            }

    return result

def delete_rule_config(zone_id, file_id):
    data = {
        "error": None,
        "data": None,
    }

    rules_dir = get_rules_dir()
    if rules_dir["error"]:
        data["error"] = rules_dir["error"]
        return data
    rules_dir = rules_dir["rules_dir"]
    config = os.path.join(rules_dir, zone_id, file_id)

    if os.path.isdir(config):
        data["error"] = "Config is a directory."
    elif not os.path.isfile(config):
        data["error"] = "Config does not exists."
    elif file_id is None:
        data["error"] = "Data must not None."
    else:
        os.remove(config)
        data["data"] = {
            "zone_id": zone_id,
	        "file_id": file_id,
            "hash_id": hash_id(zone_id, file_id),
	    }

    return data

def rename_rule_config(zone_id, file_id, new_file_id):
    result = {
        "error": None,
        "data": None,
    }
    rules_dir = get_rules_dir()
    if rules_dir["error"]:
        result["error"] = rules_dir["error"]
        return data
    rules_dir = rules_dir["rules_dir"]
    new_file_id = "".join([new_file_id, current_app.config["SHOREWALL_RULES_SUFFIX"]])
    config = os.path.join(rules_dir, zone_id, file_id)
    new_config = os.path.join(rules_dir, zone_id, new_file_id)

    if os.path.isdir(config):
        result["error"] = "Config is a directory."
    elif not os.path.isfile(config):
        result["error"] = "Config does not exists."
    elif config == new_config:
        result["error"] = "Config exits."
    elif os.path.isfile(new_config):
        result["error"] = "New config does exists."
    elif file_id is None or new_file_id is None:
        result["error"] = "Data must not None."
    else:
        try:
            copyfile(config, new_config)
            os.remove(config)
            result["data"] = {
                "message": "%s renamed to %s" % (file_id, new_file_id),
                "zone_id": zone_id,
                "file_id": new_file_id,
                "hash_id": hash_id(zone_id, new_file_id),
            }
        except IOError as error:
            result["error"] = error

    return result

def save_rule_config(zone_id, file_id, data):
    result = {
        "error": None,
        "data": None,
    }
    rules_dir = get_rules_dir()
    if rules_dir["error"]:
        result["error"] = rules_dir["error"]
        return data
    rules_dir = rules_dir["rules_dir"]
    config = os.path.join(rules_dir, zone_id, file_id)

    if os.path.isdir(config):
        result["error"] = "Config is a directory."
    elif not os.path.isfile(config):
        result["error"] = "Config does not exists."
    elif data["data"] is None:
        result["error"] = "Data must not None."
    else:
        with open(config, "w") as fd:
            fd.write(data["data"])
            result["data"] = {
                "message": "%s saved" % file_id,
                "zone_id": zone_id,
                "file_id": file_id,
                "hash_id": hash_id(zone_id, file_id),
            }

    return result

def save_config_data(file_id, to_save):
    data = {
        "error": None,
        "data": None,
    }
    config_dir = get_config_dir()
    if config_dir["error"]:
        data["error"] = config_dir["error"]
        return data
    config_dir = config_dir["config_dir"]
    config = os.path.join(config_dir, file_id) 

    if os.path.isdir(config):
        data["error"] = "Config is a directory."
    elif not os.path.isfile(config):
        data["error"] = "Config does not exists."
    elif to_save["data"] is None:
        data["error"] = "Data must not None."
    else:
        with open(config, "w") as fd:
            fd.write(to_save["data"])
            data["data"] = {
                "message": "%s saved" % file_id,
                "file_id": file_id,
                "hash_id": hash_id("", file_id),
            }

    return data


### settings ###
def get_config(type_id):
    data = {
        "error": None,
        "data": None,
    }
    config_dir = get_config_dir()
    if config_dir["error"]:
        data["error"] = config_dir["error"]
        return data
    config_dir = config_dir["config_dir"]
    config = os.path.join(config_dir, type_id) 

    if os.path.isdir(config):
        data["errro"] = "Config is a directory."
    elif not os.path.isfile(config):
        data["error"] = "Config does not exists."
    else:
        parser = Parser(config)
        data["data"] = parser.get_config(type_id)

    return data
