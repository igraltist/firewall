class Manpages(object):
    
    def list_files():
        files = {
            "Makefile": False,
            "accounting": True,
            "actions": True,
            "arprules": True,
            "blrules": True,
            "clear": True,
            "conntrack": True,
            "ecn": True,
            "findgw": True,
            "hosts": True,
            "init": True,
            "initdone": True,
            "interfaces.annotated": True,
            "lib.private": True,
            "maclist": True,
            "mangle": True,
            "masq": True,
            "masq.annotated": True,
            "nat": True,
            "netmap": True,
            "params": True,
            "policy": True,
            "policy.annotated": True,
            "providers": True,
            "proxyarp": True,
            "refresh": True,
            "refreshed": True,
            "restored": True,
            "routes": True,
            "rtrules": True,
            "rules": True,
            "scfilter": True,
            "secmarks": True,
            "shorewall.conf": True,
            "shorewall.conf.annotated": True,
            "snat": True,
            "start": True,
            "started": True,
            "stop": True,
            "stopped": True,
            "stoppedrules": True,
            "stoppedrules.annotated": True,
            "tcclasses": True,
            "tcclear": True,
            "tcdevices": True,
            "tcfilters": True,
            "tcinterfaces": True,
            "tcpri": True,
            "tunnels": True,
            "zones.annotated": True,
        }


                        
class Placeholders(object):

    def zones():
        # option common to options, in_options, and out_options
        options = ["blacklist", "ipsec", "dynamic_shared", "reqid", "spi", 
            "proto", "mss", "mode", "tunnel-src", "tunnel-dst", "strict", "next", 
        ]
        # options for type
        types = ["ipv4", "ipsec", "firewall", "bport", "vserver", "loopback",
            "local",
        ] 

        placeholder = {
            0: {
                "name": "zone",
                "options": "",
            },
            1: {
                "name": "type",
                "options": types,
            },
            2: {
                "name": "options",
                "options": options,
            },
            3: {
                "name": "in_options",
                "options": options,
            },
            4: {
                "name": "out_options",
                "options": options,
            },
        }

        return placeholder

    def interfaces():
        placeholder = {
            0: {
                "name": "zone",
                "option": "",
            },
            1: {
                "name": "interface",
                "options": "",
            },
            2: {
                "name": "options",
                "options": "",
            },
        }

        return placeholder
        

    def rules():
        placeholder = {
            0: {
                "name": "action",
                "options": "",
            },
            1: {
                "name": "source",
                "options": "",
            },
            2: {
                "name": "dest",
                "options": "",
            },
            3: {
                "name": "proto",
                "options": "",
            },
            4: {
                "name": "dest_port",
                "options": "",
            },
            5: {
                "name": "source_ports",
                "options": "",
            },
            6: {
                "name": "orginal_dest",
                "options": "",
            },
            7: {
                "name": "rate_limit",
                "options": "",
            },
            8: {
                "name": "user_group",
                "options": "",
            },
            9: {
                "name": "mark",
                "options": "",
            },
            10: {
                "name": "connlimit",
                "options": "",
            },
            11: {
                "name": "time",
                "options": "",
            },
            12: {
                "name": "headers",
                "options": "",
            },
            13: {
                "name": "switch",
                "options": "",
            },
            14: {
                "name": "helper",
                "options": "",
            },
        }

        return placeholder


