#!/usr/bin/env python
#
# Parser to get content from a config file.
#
# The parser have three main methods:
#   plain:      return plaintext
#   key_value:  return a dictonary with key, value pairs
#   complex:    return a dictonary for shorewall rules
#

"""
(c) 2007-2017 Jens Kasten <jens@kasten-edv.de>
"""

import os
import sys
import re
import argparse

from firewall.dashboard.utils import Placeholders


class Parser(object):
    """Parser for shorewall files"""

    def __init__(self, config_file):
        self.config_file = config_file
        self.fd = None
        self.config = {}
        self.placeholders = {}
        self.read()

    def read(self):
        with open(self.config_file) as fd:
            self.fd = fd.read() 

    def plain(self):
        return self.fd

    def key_value(self):
        """Return a dictionary from given config file."""
        counter = 1
        config = {}

        # remove withespace but not and arguments 
        for line in self.fd.split("\n"):
            if len(line) > 1 and not line.startswith('#'):
                # split only the first equal sign form left side
                temp = line.strip().split("=", 1)
                # check for sign '=' 
                if len(temp) == 1:
                    msg = "Missing sign '=' in %s on line %s" % \
                        (self.config_file, counter)
                    return {"error": msg}
                # remove all withespace from string
                if len(temp) == 1:
                    msg = "Missing value in %s on line %s" % \
                        (self.config_file, counter)
                    return {"error": msg}
                else:
                    value = temp[1].split("#")[0].strip()
                config[key] = value
            counter += 1
        return config

    def split_by_separator(self, line, counter, type_id):
        # split the line by multiple space or tab
        pattern = '[ \t]+'
        match = re.split(pattern, line)
        position = 0

        for i in range(len(match)):
            name = self.placeholders["header"][position]["name"]
            self.config[counter][name] = match[i]
            position += 1

    def split_by_name(self, line, counter):
        pass

    def complex(self):
        counter = 0

        for line in self.fd.split("\n"):
            if len(line) < 1 or line.startswith("#"):
                continue
            # clean string on begin and end
            line = line.lstrip()
            line = line.rstrip()
            # build a dict based on the counter 
            self.config[counter] = {}
            if line.startswith("{"):
                self.split_by_name(line, counter, "rules")
            elif line.startswith("#"):
                continue
            else:
                self.split_by_separator(line, counter, "rules")
            counter += 1 

        return self.config

    def get_config(self, type_id):
        if hasattr(Placeholders, type_id): 
            self.placeholders["header"] = getattr(Placeholders, type_id)()  
            self.config["header"] = self.placeholders["header"]
        else: 
            raise RuntimeError("Placeholders does not have: '%s'" % type_id)
       
        counter = 0
        for line in self.fd.split("\n"):

            if len(line) < 1 or line.startswith("#") or line.startswith("?"):
                continue
            # clean string on begin and end
            line = line.lstrip()
            line = line.rstrip()
            # build a dict based on the counter 
            self.config[counter] = {}
            if line.startswith("{"):
                self.split_by_name(line, counter, type_id)
            elif line.startswith("#"):
                continue
            else:
                self.split_by_separator(line, counter, type_id)
            counter += 1 
    
        return self.config


def main():
    """commandline usage"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--complex", action="store_true",
        help="Return complex")
    parser.add_argument("-p", "--plain", action="store_true",
        help="Return plain") 
    parser.add_argument("-k", "--key-value", action="store_true",
        help="Return key-value")
    parser.add_argument("config",
        help="Path for a config file.")

    args = parser.parse_args()

    if not args.config:
        parser.print_usage()
    
    config = Parser(args.config)
    if args.complex:
        for key, value in config.complex().items():
            print("%s %s" % (key, value))
    elif args.plain:
        print(config.plain())
    elif args.key_value:
        print(config.key_value)
    else:
        parser.print_help()
   

if __name__ == "__main__":
    main()
