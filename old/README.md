# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Web-UI for shorewall
* v0.2

### How do I get set up? ###
#### Dependencies ####
* virtualenv
* pip
* shorewall-5
  
For example, the '/var/www' is directory for web access.

#### create an user ####
```
#!bash
# add an user and group for the uwsgi proccess
addgroup fwadmin
adduser -g fwadmin fwadmin

# set access to home directory
chown 750 /home/fwadmin
```

Now change the rights for fwadmin.
```
#!bash
# to allow read, write, delete on config dir
chown fwadmin:fwadmin -Rv /etc/shorewall
```

Allow the user to execute shorewall with sudo without passwd.
```
#!bash
# insert follow content into /etc/sudoers.d/fwadmin
fwadmin ALL=(root) NOPASSWD: /usr/sbin/shorewall
```

#### install firewall ####
This setup using virtualenv to install python. Use the firewall admin to do this task.

```
#!bash
# setup python environ
virtualenv env-firewall
source env-firewall/bin/active

mkdir /home/fwadmin/fwconfig
mkdir /home/fwadmin/log
mkdir /home/fwadmin/database

# clone and install the firewall dependencies
git clone https://igraltist@bitbucket.org/igraltist/firewall.git
cd firewall 
pip install -r requirements
```

Now copy as root user the static content.
```
#!bash
mkdir /var/www/htdocs
cp -av /home/fwadmin/firewall/firewall/static /var/www/htdocs
```

####  nginx ####
Basic nginx setup.
```
#!bash
upstream firewall {
    server unix:/var/run/uwsgi/firewall.socket;
}

server {
    listen 0.0.0.0;
    server_name firewall;
  
    acess_log /var/log/nginx/firewall-access.log main;
    error_log /var/log/nginx/firewall-error.log info;
  
    root /var/www/htdocs;
    
    autoindex on;

    location / { 
        try_files $uri @firewall; 
    }
    
    location /static {
        alias /var/www/htdocs/static;
    }
    
    location @firewall {
        include uwsgi_params;
        uwsgi_pass firewall;
    } 
}
```

#### uwsgi ####
```
#!bash
[uwsgi]
pidfile = /var/run/uwsgi/firewall.pid
socket = /var/run/uwsgi/firewall.socket
# home directory of the firewall admin
home = /home/fwadmin
app-dir = %(home)/firewall
virtualenv = %(home)/env-firewall
chdir = %(app-dir)
mount = /firewall=run:app

env = LANG=en_US.utf8
evn = LC_ALL=de_DE.utf8
env = LC_TIME=de_DE.utf8
# have to set to fwadmin home directory
env = HOME=%(home)

manage-script-name = True
max-requests = 10
plugins = python34
processes = 1
threads = 8
```

#### Application Config ####
In the home directory of the user whom running the uwsgi process create the config directory fwconfig.
The file should be locateted in /home/fwadmin/fwconfig and named as application.cfg.

Variables from config.py which only have a string or should be empty can be overriden.

Example:
```
#!bash
# /home/fwadmin/fwconfig/application.cfg
CONFIG_TYPE = production
SECURIY_KEY = my_sec_key
# must set when using a none root user to execute shorewall commands
USE_SUDO = True
SQLALCHEMY_DATABASE_URI = sqlite:////home/fwadmin/database/database.db
LOGGING_LOCATION = /home/fwadmin/log/firewall.log
EMAIL_USE = True
EMAIL_SERVER = mail.example.com
EMAIL_SENDER = info@example.com

# this must be empty at moment
SHOREWALL_EXCLUDE_CONFIGS = 
```

#### Shorewall Rules ####
Shorewall Rules splits into different zones.
Rules located now on subdirectory rules.d instead on a single file rules.

For example I have in my zone file three zones like:
```
#!bash
# /etc/shorewall/zone
fw	firewall
net	ipv4
loc	ipv4
```

Then I created in /etc/shorewall/rules.d the follow directories:

- fw
- net
- loc

Now I have to tell the rules file to include this:
```
#!bash
# append in /etc/shorewall/rules
INCLUDE /etc/shorewall/rules.d/loc/index
INCLUDE /etc/shorewall/rules.d/fw/index
INCLUDE /etc/shorewall/rules.d/net/index
```

The index file contain follow. As example the fw index file:
```
#!bash
SHELL cat /etc/shorewall/rules.d/fw/*.rules
```

Through this all rules can be splited up for a single reason.

For example rules for teamspeak can put into:

- /etc/shorewall/rules.d/loc/teamspeak.rules

Rules for skype could be put into:

- /etc/shorewall/rules.d/loc/skype.rules


#### Shorewall Params ####

The same directory structure for Rules I use for Params. This is later needed to make build dropdown menus easier.

Then I created follow directory:
```
#!bash
mkdir /etc/shorewall/params.d
```

For example:
```
#!bash
# /etc/shorewall/params
FW=fw

INCLUDE /etc/shorewall/params.d/dns
INCLUDE /etc/shorewall/params.d/ntp
INCLUDE /etc/shorewall/params.d/hosts

# /etc/shorwall/params.d/dns
IP_DNS1=85.214.73.63
IP_DNS2=85.214.20.141
IP_DNS3=87.118.100.175
```

In the Rules then I set $IP_DNS1 to get the value:
```
#!bash
# Example rule
ACCEPT $FW net:$IP_DNS1 udp 53
```

#### Shorewall log ####
To allow the none root user to visit the logfile save them into separate directory.
I use metalog and therefore I have only a setup for this.

```
#!bash
mkdir /var/log/shorewall

# allow read the logfiles
chgrp fwadmin /var/log/shorewall
chmod 750 /var/log/shorewall
```

Change the follow in /etch/shorewall.conf
```
#!bash
LOGFILE=/var/log/shorewall/current
STARTUP_LOG=/var/log/shorewall/shorewall-start.log
# add prefix Shorewall
LOGFORMAT="Shorewall %s %s "
```

Now tell the metalog:
```
#!bash
# /etc/metalog.conf
# add for shorewall
Firewall :
    facility = "kern"
    minimum  = 6
    regex = "Shorewall"
    logdir = "/var/log/shorewall"

# add neg_regex to avoid double writes to everything and kernel like follow
Everything important :
    facility = "*"
    minimum  = 6
    neg_regex = "Shorewall"
    logdir   = "/var/log/everything"

Kernel messages :
    facility = "kern"
    logdir   = "/var/log/kernel"
    neg_regex = "Shorewall"
    break    = 1
```


All rules can created, modified, and deleted with the webui with this structure.




