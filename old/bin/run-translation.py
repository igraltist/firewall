# collect all translation
pybabel extract -F firewall/babel.cfg -o firewall/messages.pot firewall

# create po files
pybabel init -i firewall/messages.pot -d firewall/translations -l de

# after edit the po file run
pybabel compile -d firewall/translations

# for update a string
pybabel update -i messages.pot -d translations


